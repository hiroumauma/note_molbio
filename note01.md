# 分子生物学 第1講義
---

# 講義内容

DNAの分子構造と化学的性質を学び

- DNAの複製機構
- DNAの加工技術
- DNAの構造解析法 

について学習し、病気を分子生物学的観点から理解できるようにする

---

# DNAの合成

## 1, DNAの合成方法

- (1), リン酸ジエステル法
    + khorana H.G.
    + 液相法: ✖ 産物/不要物がともに液体に含まれ、分離処理が必要
- (2), ホスホトリエステル法
    + 個相法: ○
    + 収率が悪い: ✖
- (3), **ホスホロアミダイト法(Phosphoiramidite法)**
    + 個相法: ○
    + 収率改善

**ホスホロアミダイト法のしくみ**

- 3'末端となるヌクレオチド + 新たに結合するヌクレオチド
- DMT(Dimethoxytrityl期)
    + 保護基
    + 酸性条件下で遊離する

1.  最終産物では3'末端となる、最初のヌクレオシドを3'-OH基で個相担体と結合させておく。  
     + このヌクレオシドの5'-OH基はDMTで保護されている
2.  反応開始、DMT保護基を外す。カラムを酸で洗浄する
    + DMT基は色があるため光学的に効率が計測可能
3.  新たに付加するヌクレオチドを加える。
     + このヌクレオチドには3'を活性化するためホスホロアミダイト基が結合している
    + ホスホロアミダイト基:3価の亜リン酸基  
    　デオキシリボース/シアノエチル基(保護)/ジイソプロピルアミノ基  
    と結合している
    + ジイソプロピルアミノ基は容易に置換できる性質がある
    + **ジイソプロピルアミノの部分が置換されカップリング反応がおこり新たな5'3'結合が形成される**
4. 亜リン酸結合部分を要素で酸化し、ホスホトリエステルを形成する

以下 1-4のステップを繰り返すことで任意のDNAを合成する。

![](img/0101.jpg)
    

---
                                            
## 2, DNA Polymerase を用いたDNAの in vitro合成

### (1) DNA Polymerase Ⅰ

- (ア) E, coli(大腸菌)由来の酵素
    + DNA Ploymerase Ⅰ  DNAの修復, Okazaki断片のRNA部分の除去
    + DNA Ploymerase Ⅱ  DNAの修復
    + DNA Ploymerase Ⅲ  ゲノムDNA合成
    + DNA Ploymerase Ⅳ  DNA 損傷乗越DNA合成
    + DNA Ploymerase Ⅴ  〃

　  

- (イ) DNA Polymerase Ⅰ の酵素活性
    + (A) 基質 deoxynucleoside triphosphate
    + (B) 鋳型 (template)
        + 1本鎖 : 2本鎖のままでは×
    + (C) Primer


　  


- (ウ) 3'-5' *exo*nuclease 活性
    + DNA,RNA 加水分解
    + Phosphodiester結合
    + rem: ex - 外、外部
    + exodus 脱出
    + exocrine <> endocrine

　  


- (エ) 5'-3' *exo*nuclease 活性
    + 岡崎フラグメント
    + nick translation
    
　  


### (2) Taq polymerase
    + 耐熱性DNA polymerase
    + 好熱菌(Thermus aquaticus)

- PCR(Polymerase Chain Reactin) DNAの特定部分だけを増幅

**医学分野での応用**

- [1] 法医学：個人特定、親子鑑別
- [2] 微生物学：病原微生物の特定
- [3] 病理学：遺伝性疾患の診断
    
　  



---
## 3, 大腸菌を用いたDNA合成

複製起点(repliction origin) 大腸菌では oriCという 


oriCを含むプラスミドで色々複製可能

大腸菌のゲノムDNAの複製機構

- DNA helicase
- SSB(Single Stand DNA Bindingprotein)
- topoisomerase(I,II)異性化酵素
- primase
- DNA polymereseⅢ
- DNA polymeraseI
- RNaseH
- DNA Ligase    

