# DNA合成
## 1, DNAの有機合成法
### (1) リン酸ジエステル法
        khorana H.G.
        ×　液相法　…　 産物や不要物がすべて同じ液体にとけるため分離処理が必要

### (2) ホスホトリエステル法
        〇　固相法
        ×　収率が悪い！

### (3) ホスホロアミダイト法(Phosphoiramidite法)
        〇　固相法
![](1.png)

DNAの合成で可能になった分子生物学的技術
- (a) 遺伝子の人工合成
- (b) サンガー法によるDNAの塩基配列の解析 Sequence primer
- (c) DNAへの人工突然変異の導入
- (d) 相補的塩基配列(DNA,RNA)を検出

![](2.png)

## 2, DNA Polymerase を用いたDNAの in vitro合成
### (1) DNA Polymerase Ⅰ
#### (ア) E, coli(大腸菌)由来の酵素
- DNA Ploymerase Ⅰ  DNAの修復, Okazaki断片のRNA部分の除去
- DNA Ploymerase Ⅱ  DNAの修復
- DNA Ploymerase Ⅲ  ゲノムDNA合成
- DNA Ploymerase Ⅳ  DNA 損傷乗越DNA合成
- DNA Ploymerase Ⅴ  〃


#### (イ) DNA Polymerase Ⅰ の酵素活性
##### (A) 基質 deoxynucleoside triphosphate
![](3.png)

##### (B) 鋳型 (template)
    1本鎖 : 2本鎖のままでは×

##### (C) Primer
![](4.png)
![](5.png)

#### (ウ) 3'-5' *exo*nuclease 活性
    DNA,RNA 加水分解
    Phosphodiester結合

        rem: ex - 外、外部
            exodus 脱出
            exocrine <> endocrine
![](6.png)

#### (エ) 5'-3' *exo*nuclease 活性
